# Zadání SP - Super-resolution
Zadáním je vytvoření modelu, který převede low-resolution obrázek na high-resoltuion obrázek (Super-resolution). Model by měl zvětši obrázek 4x z každé strany, celkově to je teda 16x více pixelů. Celkově jsem naimplementoval 10 různých modelů (2x SRCNN, 8x diffusion, které mají různé noise schedulers).

## Použité modely
- SRCNN (officiální podle článku, můj navržený)
- Diffusion models (8 různých variantt, které se liší noise schedulingem a inputem)

## Stažení dat
Data byla stažena ze stránky kaggle: https://www.kaggle.com/datasets/quadeer15sh/image-super-resolution-from-unsplash. Stažená verze se nachází na disku, který byl nasdílen, popřípadě je potřeba dataset stáhnout a uložit ho do rootu projektu pod názvem `dataset2/`.

## Spuštění
Implementace a také dataset se nachází na sdíleném google disku https://drive.google.com/drive/folders/0AJJqBLonmCmmUk9PVA. Disk obsahuje colab, kde je implementace všech 10 modelů.  Doporučuji spustit první buňku, následně restartovat běhobé prostředí (colab to vyloženě napíše) a poté spouštět ostatní. První buňka je pouze pro upřesnění verze knihovny, abych mohl používat novější funkce. Disk také obsahuje natrénované modely a jejich nejúspěšnější varianty, dále také diffusion gify, které se nacházejí ve složce `models/diffusion_outputs`.
